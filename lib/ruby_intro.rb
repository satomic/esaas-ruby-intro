# When done, submit this entire file to the autograder.

# Part 1

def sum(arr)
  # Using inject:
  arr.inject(0){|total, val| total+val}
end

def max_2_sum(arr)
  case arr.length
    # Return 0 if array is empty:
    when 0
      return 0
    # Return value of the element if just one element:
    when 1
      return arr[0]
    else
      # Generate combination, reduce it through sum, then select the max:
      sums = arr.combination(2).to_a.map {|comb| comb.reduce(0, :+)}
      return sums.max
  end
end

def sum_to_n? arr, n
  case arr.length
    # Return false for empty array & single element array:
    when 0, 1
      return false
    else
      # Generate combination, reduce it through sum, then filter it:
      sums = arr.combination(2).to_a.map {|comb| comb.reduce(0, :+)}
      qualified_sums = sums.select {|val| val == n}
      return 0 != qualified_sums.length
  end
end

# Part 2

def hello(name)
  # String concatenation:
  "Hello, "+name
end

def starts_with_consonant?(s)
  # Vowels:
  s.downcase!
  return false if s.empty?||(s[0] < 'a')||(s[0]>'z')
  vowels = Set['a', 'e', 'i', 'o', 'u']
  # Is it starts with consonant:
  not vowels.include?(s[0])
end

def binary_multiple_of_4? s
  return false unless /^[01]+$/ =~ s
  case s.length
  when 1
    return true unless s != '0'
  else 
    return true unless s[-2..-1] != '00'
  end
  return false
end

# Part 3

class BookInStock
  
  attr_reader :isbn, :price
  
  def initialize(isbn, price)
    raise ArgumentError.new("isbn shouldn't be empty") if isbn.empty?
    raise ArgumentError.new("price shouldn't be less than zero") if price <= 0.0
    @isbn,@price = isbn, price
  end
  
  def isbn=(value)
    raise ArgumentError.new("isbn shouldn't be empty") if value.empty?
    @isbn = value
  end
  
  def price=(value)
    raise ArgumentError.new("price shouldn't be less than zero") if value <= 0.0 
    @price = value
  end
  
  def price_as_string
    sprintf("$%.2f", @price)
  end
end
